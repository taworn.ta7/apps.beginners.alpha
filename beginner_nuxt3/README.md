# beginner_nuxt3

Beginner code for Nuxt 3.


## Features

### Client

* Composition API
* 2 Languages, English and Thai
* MessageBox
* WaitBox
* MenuBox

### Server

* Logging with colorize
* Config files, .env and .env.override to edit
* HTTP exception with more information
* Background task scheduled to delete unused data table(s)


## Run Web

To load packages, just run this once:

	npm i

To run:

	npm run dev

Open browser and type URL box:

	localhost:8088


## Credits

Thank you, [Google Fonts](https://fonts.google.com/icons) API.

Thank you, [Melvin ilham Oktaviansyah](https://freeicons.io/profile/8939) on [freeicons.io](https://freeicons.io) for icons.

Thank you, [icon king1](https://freeicons.io/profile/3) on [freeicons.io](https://freeicons.io) for icons.


## Last

Sorry, but I'm not good at English. T_T


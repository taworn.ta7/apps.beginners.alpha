// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devtools: { enabled: true },
    compatibilityDate: '2024-07-05',
    nitro: {
        plugins: [
            './plugins/bootstrap.ts',
            './plugins/logging.ts',
        ],
    },
    modules: [
        '@nuxtjs/i18n',
    ],
    i18n: {
        vueI18n: './i18n.config.ts',  // if you are using custom path, default
    },
})

import * as path from 'node:path'
import * as fs from 'node:fs'
//import { logger } from '../utils/logger'
//import { daysToKeepToDate, dateFormat } from '../utils/task_utils'

export default async function clearLogs(daysToKeep: number, folder: string): Promise<void> {
	// checks before execute, days to keep must more than zero
	if (daysToKeep <= 0)
		return

	// computes date range
	const date = daysToKeepToDate(daysToKeep)

	// cleans obsolete data
	logger.debug(`logs older than ${dateFormat(date)} will be delete!`)
	const files = await fs.promises.readdir(folder)
	const re = /^([0-9]{4})([0-9]{2})([0-9]{2})\.log$/
	for (let i = 0; i < files.length; i++) {
		const file = files[i]
		const found = file.match(re)
		if (found) {
			const d = new Date(Number(found[1]), Number(found[2]) - 1, Number(found[3]))
			if (d.valueOf() < date.valueOf()) {
				logger.debug(`delete: ${file}`)
				await fs.promises.rm(path.join(folder, file))
			}
		}
	}
}

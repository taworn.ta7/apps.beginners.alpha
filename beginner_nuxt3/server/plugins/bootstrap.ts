/*import dotenv from 'dotenv'
dotenv.config({
	path: [
		'.env.override',
		'.env',
	],
})
*/
import * as path from 'node:path'
import * as url from 'node:url'
//import { consola } from 'consola'
//import { logger } from '../utils/logger'
import clearLogs from '../tasks/clear_logs'

const delayTime = 1
const intervalTime = 8 * 60 * 60 * 1000

export default defineNitroPlugin((nitroApp) => {
	const __filename = url.fileURLToPath(import.meta.url)
	const __dirname = path.dirname(__filename)
	const env = <any>process.env

	// initializing tasks
	const logDir = path.resolve(path.join(__dirname, '..', '..', env.LOG_DIR))
	logger.log('verbose', `logs folder: ${logDir}`)
	logger.verbose(`logs to console: ${!!+env.LOG_TO_CONSOLE}`)
	logger.verbose(`logs to file: ${!!+env.LOG_TO_FILE}`)
	logger.debug(`days to keep logs: ${+env.DAYS_TO_KEEP_LOGS} day(s)`)
	logger.debug(`days to keep dummy data: ${+env.DAYS_TO_KEEP_DUMMY} day(s)`)

	// clearing function
	const callback = async () => {
		try {
			await clearLogs(+env.DAYS_TO_KEEP_LOGS, logDir)
			//await clearDummy(+env.DAYS_TO_KEEP_DUMMY)
			//
			// clearing more out of used data
			//
		}
		catch (e) {
			logger.error(`uncaught exception: ${e}`)
		}
		setTimeout(callback, intervalTime)
	}
	setTimeout(callback, delayTime)
})

export function daysToKeepToDate(daysToKeep: number): Date {
	const now = new Date()
	const nowNoTime = new Date(now.getFullYear(), now.getMonth(), now.getDate())
	return new Date(nowNoTime.valueOf() - (daysToKeep * 24 * 60 * 60 * 1000))
}

export function dateFormat(d: Date): string {
	return d.getFullYear() + '-' + (d.getMonth() + 1).toString().padStart(2, '0') + '-' + d.getDate().toString().padStart(2, '0')
}

import * as path from 'node:path'
import * as fs from 'node:fs'
import * as url from 'node:url'
import { Logger, createLogger, format, transports } from 'winston'
// https://github.com/winstonjs/winston-daily-rotate-file/issues/90
// have to done this, see user 'staplespeter'
import DailyRotateFile from 'winston-daily-rotate-file'

function setup(): Logger {
	const __filename = url.fileURLToPath(import.meta.url)
	const __dirname = path.dirname(__filename)
	const env = <any>process.env

	const logDir = path.resolve(path.join(__dirname, '..', '..', env.LOG_DIR))
	if (!fs.existsSync(logDir)) {
		fs.mkdirSync(logDir)
	}

	let xports = []
	if (+env.LOG_TO_CONSOLE) {
		xports.push(new transports.Console({
			format: format.combine(
				format.colorize({
					all: true,
					colors: {
						error: 'brightWhite bgRed',
						warn: 'brightRed',
						info: 'brightWhite',
						http: 'brightCyan',
						verbose: 'brightBlue',
						debug: 'brightGreen',
						silly: 'white',
					},
				}),
			),
		}))
	}
	if (+env.LOG_TO_FILE) {
		//xports.push(new transports.DailyRotateFile({
		xports.push(new DailyRotateFile({
			filename: `${logDir}/%DATE%.log`,
			datePattern: 'YYYYMMDD',
		}))
	}

	const logger = createLogger({
		level: process.env.NODE_ENV === 'production' ? 'info' : 'silly',
		format: format.combine(
			format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
			format.printf(info => `${info.timestamp} ${info.level.substring(0, 3).toUpperCase()} ${info.message}`),
		),
		transports: xports,
	})
	/*
	logger.silly("Testing silly message.")
	logger.debug("Testing debug message.")
	logger.verbose("Testing verbose message.")
	logger.http("Testing http message.")
	logger.info("Testing info message.")
	logger.warn("Testing warn message.")
	logger.error("Testing error message.")
	*/

	return logger
}

// ----------------------------------------------------------------------

export const logger: Logger = setup()


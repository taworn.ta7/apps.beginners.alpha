# beginner_tauri1_vue3

Beginner code for Tauri 1.4 + Vue 3.


## Features

* Composition API
* 2 Languages, English and Thai
* MessageBox
* WaitBox
* MenuBox


## Run App

To load packages, just run this once:

	npm i

To run:

	npm run tauri dev


## Credits

Thank you, [Google Fonts](https://fonts.google.com/icons) API.

Thank you, [Melvin ilham Oktaviansyah](https://freeicons.io/profile/8939) on [freeicons.io](https://freeicons.io) for icons.

Thank you, [icon king1](https://freeicons.io/profile/3) on [freeicons.io](https://freeicons.io) for icons.


## Last

Sorry, but I'm not good at English. T_T


import { defineStore } from 'pinia'
import { inject } from 'vue'
import { type MessageBoxOptions } from '../components/MessageBox'
import { type WaitBoxOptions } from '../components/WaitBox'

/**
 * Application singleton store class.
 */
export const useAppStore = defineStore({

	id: 'AppStore',

	state: () => ({

		/**
		 * Injects i18n.
		 */
		i18n: inject('i18n'),

		/**
		 * Current locale.
		 */
		locale: '',

		/**
		 * Availables theme list.
		 */
		themeList: [
			'blue',
			'green',
			'orange',
		],

		/**
		 * Current theme name.
		 */
		theme: '',

		/**
		 * Availables tone list.
		 */
		toneList: [
			'default',
			'dark',
			'lite',
		],

		/**
		 * Current tone name.
		 */
		tone: '',

		/**
		 * Current dark mode.
		 */
		dark: false,

		/**
		 * A DialogBox instance.  Must be setupUi() in AppBox first.
		 */
		dialogBox: <any>null,

		//
		// more state variables here...
		//

	}),

	actions: {

		/**
		 * Begin current session.
		 */
		setup(): void {
			// NOTE:
			// comment this line(s) if you want to use other save feature
			this.setLocale(localStorage.getItem('locale') || 'en')
			this.setTheme(localStorage.getItem('theme') || '')
			this.setTone(localStorage.getItem('tone') || '')
		},

		/**
		 * Installs DialogBox before used.
		 */
		setupUi(dialogBox: any): void {
			this.dialogBox = dialogBox
		},

		// ----------------------------------------------------------------------

		/**
		 * Changes current locale.
		 */
		setLocale(locale: string): void {
			// set current locale
			this.locale = locale
			this.i18n.global.locale = locale
			console.log(`current locale: ${this.i18n.global.locale}`)

			// NOTE:
			// comment this line(s) if you want to use other save feature
			localStorage.setItem('locale', this.i18n.global.locale)
		},

		/**
		 * Changes current theme.
		 */
		setTheme(theme: string): void {
			// validates parameter and reset theme to default if it is not right
			const index = this.themeList.findIndex((e: string) => e === theme)
			if (index < 0 || index >= this.themeList.length)
				theme = this.themeList[0]

			// set theme
			const root = document.querySelector(':root') as any
			const style = root.style
			style.setProperty(`--c-top-header-bg`, `var(--c-${theme}-top-header-bg)`)
			style.setProperty(`--c-top-header-fg`, `var(--c-${theme}-top-header-fg)`)
			//
			// NOTE:
			// add more property here...
			//
			document.documentElement.setAttribute('data-theme', theme)
			this.theme = theme
			console.log(`current theme: ${this.theme}`)

			// NOTE:
			// comment this line(s) if you want to use other save feature
			localStorage.setItem('theme', this.theme)
		},

		/**
		 * Changes current tone.
		 */
		setTone(tone: string): void {
			// validates parameter and reset tone to default if it is not right
			const index = this.toneList.findIndex((e: string) => e === tone)
			if (index < 0 || index >= this.toneList.length)
				tone = this.toneList[0]

			// set tone
			const root = document.querySelector(':root') as any
			const style = root.style
			style.setProperty(`--mode`, `var(--${tone}-mode)`)
			style.setProperty(`--c-background`, `var(--c-${tone}-background)`)
			style.setProperty(`--c-background-soft`, `var(--c-${tone}-background-soft)`)
			style.setProperty(`--c-background-mute`, `var(--c-${tone}-background-mute)`)
			style.setProperty(`--c-border`, `var(--c-${tone}-border)`)
			style.setProperty(`--c-border-hover`, `var(--c-${tone}-border-hover)`)
			style.setProperty(`--c-heading`, `var(--c-${tone}-heading)`)
			style.setProperty(`--c-text`, `var(--c-${tone}-text)`)
			style.setProperty(`--c-hilite-text`, `var(--c-${tone}-hilite-text)`)
			style.setProperty(`--c-link`, `var(--c-${tone}-link)`)
			style.setProperty(`--c-link-visited`, `var(--c-${tone}-link-visited)`)
			style.setProperty(`--c-link-active`, `var(--c-${tone}-link-active)`)
			//
			// NOTE:
			// add more property here...
			//
			document.documentElement.setAttribute('data-tone', tone)
			this.tone = tone
			const mode = window.getComputedStyle(root).getPropertyValue(`--mode`)
			this.dark = mode == 'dark'
			console.log(`current tone: ${this.tone}, dark mode: ${this.dark}`)

			// NOTE:
			// comment this line(s) if you want to use other save feature
			localStorage.setItem('tone', this.tone)
		},

		// ----------------------------------------------------------------------

		// NOTE:
		// These functions required DialogBox installed.

		/**
		 * Checks if the message box is opened or not.
		 */
		messageBoxIsOpen: function (): boolean {
			return this.dialogBox?.messageBoxIsOpen()
		},

		/**
		 * Opens message box.
		 */
		messageBoxOpen: function (options: MessageBoxOptions): boolean {
			return this.dialogBox?.messageBoxOpen(options)
		},

		/**
		 * Opens infomation box.
		 */
		messageBoxInfo: function (content: string, callback?: (result: boolean) => void): boolean {
			return this.dialogBox?.messageBoxInfo(content, callback)
		},

		/**
		 * Opens warning box.
		 */
		messageBoxWarning: function (content: string, callback?: (result: boolean) => void): boolean {
			return this.dialogBox?.messageBoxWarning(content, callback)
		},

		/**
		 * Opens error box.
		 */
		messageBoxError: function (content: string, callback?: (result: boolean) => void): boolean {
			return this.dialogBox?.messageBoxError(content, callback)
		},

		/**
		 * Opens question box.
		 */
		messageBoxQuestion: function (content: string, callback?: (result: boolean) => void): boolean {
			return this.dialogBox?.messageBoxQuestion(content, callback)
		},

		/**
		 * Checks if the wait box is opened or not.
		 */
		waitBoxIsOpen: function (): boolean {
			return this.dialogBox?.waitBoxIsOpen()
		},

		/**
		 * Opens wait box.
		 */
		waitBoxOpen: function (o?: WaitBoxOptions): boolean {
			return this.dialogBox?.waitBoxOpen(o)
		},

		/**
		 * Closes the wait box.
		 */
		waitBoxClose: function (): boolean {
			return this.dialogBox?.waitBoxClose()
		},

		/**
		 * Opens wait box and closed when the callback function is returned.
		 */
		waitBoxAsync: async function <T>(callback: () => Promise<T>): Promise<T> {
			this.dialogBox.waitBoxOpen()
			const ret = await callback()
			this.dialogBox.waitBoxClose()
			return ret
		},

		/**
		 * Checks if the menu box is opened or not.
		 */
		menuBoxIsOpen: function (): boolean {
			return this.dialogBox?.menuBoxIsOpen()
		},

		/**
		 * Opens menu box.
		 */
		menuBoxOpen: function (): boolean {
			return this.dialogBox?.menuBoxOpen()
		},

		/**
		 * Closes the menu box.
		 */
		menuBoxClose: function (): boolean {
			return this.dialogBox?.menuBoxClose()
		},

		// ----------------------------------------------------------------------

		//
		// more actions here...
		//

	},

})

import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../pages/HomePage.vue'

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes: [
		{
			path: '/',
			name: 'home',
			component: HomePage,
		},
		{
			path: '/test-box',
			name: 'test-box',
			component: () => import('../pages/TestBoxesPage.vue'),
		},
		{
			path: '/test-box-shared',
			name: 'test-box-shared',
			component: () => import('../pages/TestBoxesSharedPage.vue'),
		},
	]
})

export default router
